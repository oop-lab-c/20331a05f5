//Java Program to demonstrate the userdefined exceptions
import java.util.*;

 class MyException extends Exception
{
  
  MyException(int n)
  {
    System.out.println("age must be above 18");
  }
}
class UserExHandJava
{
  public static void main(String[] args)
  {
  System.out.println("Enter your age: ");
   Scanner in = new Scanner(System.in); //Reading the input from the user
   int age = in.nextInt();
    if(age > 18)
    {
      System.out.println("eligible to vote");
    }
   else{
      try                               //try block
      {
        throw new MyException(age);
      }catch(Exception e)               //catch block
      {
       System.out.println(e);
      }
      finally                            //finally block
      {
        System.out.println("Exception Handled..");
      }
    }
  }
}