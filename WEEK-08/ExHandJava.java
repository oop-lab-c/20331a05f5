//Java Program to demonstrate built-in exception
import java.util.*;
public class ExHandJava
{
    public static void main(String [] args)
    {
    int a=5,b=0,c;
    try 
    {
        c=a/b;
        System.out.println(c);
    } catch (ArithmeticException e) //handles Arithmetic exception
    {  
        System.out.println(e);
        System.out.println("can't divide by zero");
    }
    try
    {
        int k[] = new int[10];
        k[11]=5;
        System.out.println(k[11]);
    }catch(ArrayIndexOutOfBoundsException e)  //handles Array out of bounds exception
    {
        System.out.println(e);
        System.out.println("Array out of bounds exception handled...");
    }
    try
    {
        String str = null;
        System.out.println(str.length());
    }catch(NullPointerException e) //handles null pointer exceptions
    {
        System.out.println(e);
        System.out.println("Null Pointer Exception handled...");
    }
    finally
    {
        System.out.println("Runtime Exceptions are handled...");
    }
    }
    
}