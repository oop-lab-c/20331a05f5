// C++ Program to access member variables using setVar() and getVar()
#include <iostream>
using namespace std;
class AccessSpecifierDemo
{
  private:
    int priVar; //private variable
  protected:
    int proVar; //protected variable
  public:
    int pubVar; // public variable
 void setVar(int priValue, int proValue,int pubValue) // setVar() is used for setting the values from the user
{
  priVar = priValue;
  proVar = proValue;
  pubVar = pubValue;
}
void getVar() //getVar() is used to get the values which have been set
{
  cout << "Private : " << priVar << endl;
  cout << "Protected : " << proVar << endl;
  cout << "Public : " << pubVar << endl;
}
};
int main()
{
 AccessSpecifierDemo obj;
 int pri,pro,pub;
  cout << "enter private,protected and public : " << endl;
  cin >> pri >> pro >> pub ; //Taking input from the user
  obj.setVar(pri,pro,pub); 
  obj.getVar();
}
