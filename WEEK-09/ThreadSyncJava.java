//Java Program to Demonstrate the usage of Synchronization of threads.
class Table
{
    synchronized void printTable(int n) //synchronized method 
    {
    for (int i=1;i<=10;i++)
    {
    System.out.println(n*i);
    try
    { 
      Thread.sleep(400);      //sleep method
    } catch(Exception e) 
    {
      System.out.println(e);           //Interrupted exception
    }
    }
   }
}
class MyThread1 extends Thread    //thread 1
{
  Table obj;
  MyThread1(Table obj1)
  {
    obj=obj1;
  }
 public void run()
 {
     obj.printTable(5);
 }
}
class MyThread2 extends Thread  //thread 2
{
Table obj2;
MyThread2(Table obj3)
{
  obj2=obj3;
}
 public void run()
 {
     obj2.printTable(3);
 }
}
public class ThreadSyncJava
{
    public static void main(String[] args)
    {
    Table ob = new Table();
     
     MyThread1 ob1 = new MyThread1(ob); 
     MyThread2 ob2 = new MyThread2(ob);
     ob1.start();    //start method
     ob2.start();
    }
}
