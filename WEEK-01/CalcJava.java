// Java Program to perform Arithmetic operations
import java.util.*;
class CalcJava
{
    public static void main(String [] args)
    {
        Scanner input = new Scanner(System.in); // To take input from user
        System.out.println("Enter a: ");
        int a = input.nextInt(); //Reads and Take integer value from user
        System.out.println("Enter b: ");
        int b = input.nextInt(); //Reads and Take integer value from user
        System.out.println("Enter your choice: ");
        char op = input.next().charAt(0); //Reads and Take character from user
        if(op == '+')
        System.out.println("Addition of two numbers : " + (a+b)); // Results Addition of two numbers
        else if(op == '-')
        System.out.println("Difference of two numbers : " + (a-b)); // Results Difference of two numbers
        else if(op == '*')
        System.out.println("Product of two numbers : "+ (a*b)); // Results Product of two numbers
        else if(op == '/')
        System.out.println("Quotient of two numbers : "+(a/b)); // Results Quotient of two numbers
        else if(op == '%')
        System.out.println("Remainder of two numbers : "+(a%b)); // Results Remainder of two numbers
        else
        System.out.println("The operator you have choosen is invalid");
    }
}
