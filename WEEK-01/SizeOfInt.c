//Program to display size of an integer
#include<stdio.h>
void main()
{
    printf("Size of int : %d", sizeof(int)); //sizeof() is a built-in function, used for finding the size of a data type
}