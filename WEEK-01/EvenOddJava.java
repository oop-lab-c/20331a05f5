//Java Program to check whether the given number is even or odd
import java.util.*;
class EvenOddJava
{
    void EvenOdd(int num)
    {
        if(num % 2 == 0)
        System.out.println(num + " is an even number."); // Results when the number is even
        else
        System.out.println(num + " is an odd number."); // Results when the number is odd
    }
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in); //To take input from user
        System.out.println("Enter a number: ");
        int num = input.nextInt();
        EvenOddJava obj = new EvenOddJava(); // new keyword is to used to create object for class
        obj.EvenOdd(num);
    }
}

