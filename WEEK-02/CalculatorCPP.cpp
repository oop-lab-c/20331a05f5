//C++ program to compute arithmetic operations with the user input and choice of operation(switch case).
#include<iostream>
using namespace std;
int main()
{
    int a,b;
    char op;
    cout << "Enter a: "; // the value of a
    cin >> a; // taking the value of a from the user
    cout << "Enter b: "; // the value of b
    cin >> b; // taking the value of b from the user
    cout << "Enter your choice(+,-,*,/,%) : " ;
    cin >> op; // taking the operator from the user
    switch(op)
    {
        case '+': cout << "sum : " << a+b << endl; // prints sum of two numbers
        break; 
        case '-': cout << "Difference : " << a-b << endl; //prints difference of two numbers
        break;
        case '*': cout << "product : " << a*b << endl; //prints product of two numbers
        break;
        case '/': cout << "Quotient : " << a/b << endl; //prints quotient of two numbers
        break;
        case '%': cout << "Remainder : " << a%b << endl; // prints remainder of two numbers
        break;
    }
    return 0;
}
