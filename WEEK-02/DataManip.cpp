//Program to demonstrate the usage of datamanipulators(endl,ends,ws,flush,setw,setfill,setprecision)
#include<iostream>
#include<iomanip>
#include<istream>
#include<sstream>
#include<string>
using namespace std;                                    
int main()
{
    int x=1, y=2;
    float z=3.5678;
    cout << "hello" << endl
     << "test" <<  setw(4) << setfill('*') << x <<  endl //setw (int n) – To set field width to n and setfill (Char f) – To set the character to be filled
    << "exam" << setw(4) << y << endl      //  setw (int n) – To set field width to n and endl – Gives a new line
    << setprecision(2) << z << flush <<endl; //setprecision (int p) – The precision is fixed to p and flush – Flushes the buffer stream
    istringstream str("       welcome");
    string line;
    getline(str >> std :: ws, line); //ws – Omits the leading white spaces present before the first field
    cout << line << endl;
    cout << "a" ;
    cout << "b" << ends << "/"; //ends – Adds null character to close an output string
    return 0;
}