//Program to display hello username
#include<iostream>
using namespace std;
int main()
{
    string name;
    cout << "Enter your name : ";
    cin >> name; //Taking input from the user
    cout << "Name : " << name << endl; //prints username
    cout << "Hello " + name << endl; //Display username along with Hello
    return 0;
}