//Program to demonstrate the usage of parameterized constructor
import java.util.*;
class Student
{
   String collegeName,name;
   int collegeCode;
   double percentage;
Student(String fullName, double semPercentage)  // parameterized constructor
 {
   collegeName = "MVGR"; 
   collegeCode = 33;
   name = fullName;
   percentage = semPercentage;
 }   
 void display()
 {
   System.out.println("college name : " +collegeName);
    System.out.println("college code : " +collegeCode); 
    System.out.println("Full Name : " +name);
    System.out.println("Sem percentage : " +percentage);
 }
}
class ParamConstJava
{
 public static void main (String[] args)
 {
     System.out.println("Enter your name and percentage : ");
     Scanner input = new Scanner(System.in); //reading the input from the user
     String fullName = input.nextLine();
     double semPercentage = input.nextDouble();
     Student obj  = new Student(fullName,semPercentage);
     obj.display();
 }

}