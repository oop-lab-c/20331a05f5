//C++ Program to demonstrate diamond problem
#include<iostream>
using namespace std;
//hierarchial inheritence
class Person 
{ 
public:
    Person()
      {
           cout << "Person class is called" << endl;
      }
};
 
class Father : public Person //Father is derived from Person
{ 
public:
    Father()
    {
       cout << "Father class is called" << endl;
    }
};

class Mother : public Person //Mother is derived from Person 
{ 
public:
    Mother()
    {
        cout << "Mother class is called" << endl;
    }
};
  //Multiple inheritence
class Child : public Father, public Mother //child is derived from both father and mother
{ 
public:
    Child() 
    {
        cout << "Child class is called" << endl;
    }
};
 
int main() 
{
    Child obj;
    return 0;
}
