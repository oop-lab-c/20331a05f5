//C++ Program to demonstrate the usage of simple inheritence
#include<iostream>
using namespace std;
class Operation
{
    public:
    void operation(int a, int b)
    {
        cout << "logical and : " << ((a<5) && (b>0)) << endl;
        cout << "logical or : " << ((a>5) || (b>3))<<endl;
        cout << "logical not : " << !a << endl;
    }
};
class Logical : public Operation
{};
int main()
{
    int a,b;
    cout << "enter a and b: " << endl; //Reading input from the user
    cin >> a >> b ;
    Logical obj;
    obj.operation(a,b);
    return 0;
}
