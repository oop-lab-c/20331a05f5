//program to Demonstrate public, private and protected inheritence in java
import java.util.*;
class Public     //public base class
{
    void fun()
    {
        System.out.println("public base class");
    }
}
class PublicDerived extends Public{} // public derived class
/*private class Private
{
    void fun1()
    {
        System.out.println("private base class");
    }
} 
private class PrivateDerived extends Private{}*/
/*protected class Private
{
    void fun2()
    {
        System.out.println("protected base class");
    }
}  
protected class ProtectedDerived extends Protected{}*/
public class PPPInheriJava {
    public static void main(String[] args)
    {
    PublicDerived obj = new PublicDerived();
    obj.fun();
    //PrivateDerived obj1 = new privateDerived();
    //obj1.fun1();
    //ProtectedDerived obj2 = new ProtectedDerived();
    //obj2.fun2();
    }
}
