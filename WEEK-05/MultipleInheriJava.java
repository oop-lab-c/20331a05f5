//Java Program to demonstrate the diamond problem
import java.util.*;
//hierarchial inheritence
class Base         // Base class
{
    void fun()
    {
        System.out.println("Base class method is called");
    }
}
class Derived1 extends Base        // Derived1 class extends the properties of Base class
{
    void fun1()
    {
        System.out.println("Derived1 class method is called");
    }
}
class Derived2 extends Base         // Derived2 class extends the properties of Base class
{
  void fun2()
  {
      System.out.println("Derived2 class method is called");
  }
}
//multiple inheritence
class child extends Derived1,Derived2  //child class is trying to extend the properties of Derived1 and Derived2
{
    void func()
    {
    System.out.println("child class method is called");
    }
}
public class MultipleInheriJava 
{
  public static void main (String[] args)
  {
    child obj;
    obj.func();
    obj.fun1();
    obj.fun2();
  }    
}
